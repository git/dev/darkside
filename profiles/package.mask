# Jeremy Olexa <darkside@gentoo.org> (5 Sept 2013)
# Mask higher versions because something is broken with it (for me). See
# https://bugs.gentoo.org/483576 . 2.6.39 was stabilized for existing CVE but
# that only applies if you have Opportunistic Encryption Support enabled which I
# do not (and neither do most people).
# http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2013-2053
>=net-misc/openswan-2.6.39
